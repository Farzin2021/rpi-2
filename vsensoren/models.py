from django.db import models
from heizmanager.fields import ListField
from heizmanager.models import Raum, Gateway, GatewayAusgang, Sensor, HausProfil, AbstractSensor, AbstractDevice
from heizmanager import cache_helper as ch
from heizmanager import pytz
from datetime import datetime
from django.db.models.signals import post_init, post_delete, post_save
from django.dispatch import receiver
from caching.base import CachingManager
from heizmanager.models import sig_new_temp_value


class VSensor(AbstractDevice, AbstractSensor):
    input_sensoren = ListField(models.CharField(), default=[])  # koennen IDs von Sensoren und VSensoren sein
    verknuepfung = models.CharField(max_length=127, blank=True)

    wert = models.FloatField(null=True)

    gateway = models.ForeignKey('heizmanager.Gateway', related_name='vsensoren', null=True)  # das gateway vom backup sensor

    # VSensor darf nur einem Ausgang an dem GW von diesem Sensor zugeordnet werden!
    backup_sensor = models.ForeignKey('heizmanager.Sensor', null=True)

    remote_id = models.BigIntegerField(default=0)
    
    objects = CachingManager()

    def __unicode__(self):
        return self.description

    def delete(self, *args, **kwargs):
        try:
            if self.mainsensor:
                self.raum.regelung.gatewayausgang.delete()
        except (Raum.DoesNotExist, GatewayAusgang.DoesNotExist, AttributeError):
            pass
        for vsensor in self.haus.vsensor_related.all():
            if self.get_identifier() in vsensor.input_sensoren:
                vsensor.input_sensoren = [str(s) for s in vsensor.input_sensoren if s != self.get_identifier()]
                vsensor.save()
        if self.mainsensor:
            ch.delete('mainsensor_%s' % self.raum_id)
        VSensor.objects.filter(pk=self.pk).delete()

    def save(self, *args, **kwargs):
        super(VSensor, self).save(*args, **kwargs)
        if self.name is not None and self.name != '':
            ch.delete(self.name)  # damit wert neu berechnet wird
            if self.mainsensor:
                ch.set('mainsensor_%s' % self.raum_id, self)
            else:
                ch.delete('mainsensor_%s' % self.raum_id)  # vorsorglich loeschen

    def get_input_sensoren(self):
        sensoren = []
        todel = []
        for i_s in self.input_sensoren:
            sensor = AbstractSensor.get_sensor(i_s)
            if sensor is None and 'VSensor' in i_s:
                try:
                    _i_s = i_s.split('*')[1]
                    sensor = VSensor.objects.get(pk=long(_i_s))
                except VSensor.DoesNotExist:
                    pass
            if sensor:
                sensoren.append(sensor)
            else:
                todel.append(i_s)

        if len(todel):
            for s in todel:
                self.input_sensoren.remove(s)
            self.save()

        return sensoren

    def get_wert(self):
        '''
        1: minimum: min([%(s1.id)s,%(s2.id)s,...])
        2: maximum: analog
        3: median: sorted([1,2,...])[len([1,2,...])/2]
        4: avg: sum([])/len([])
        5: ohne kleinsten wert: sum(sorted([1,2,3,4])[1:])
        6: ohne groessten wert: sum(sorted([1,2,3,4])[:len([])-1])
        7: ohne kleinsten und groessten wert: analog

        wenn None zurueckkommt, dann sollte der aufrufer wissen, dass er sich dann an den backup wenden muss.
        '''

        last = ch.get_last_temp(self.name)
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        if last is not None:
            gwdelay = HausProfil.objects.get(haus_id=long(last[2])).get_gateway_delay()
            if (now-last[1]).seconds < gwdelay*1.5:
                return last

        werte = {}
        for input_sensor in self.get_input_sensoren():
            wert = input_sensor.get_wert()
            marker = input_sensor.get_marker()
            if marker != 'green':
                continue
            if wert is None:
                werte[str(input_sensor.get_identifier())] = None
            else:
                werte[str(input_sensor.get_identifier())] = wert[0]
        if not len(werte) or None in werte.values():
            if self.backup_sensor_id:
                try:
                    last = self.backup_sensor.get_wert()
                    if last and (now-last[1]).total_seconds() < 1800:
                        return last[0], now, self.haus_id
                except Sensor.DoesNotExist:
                    self.backup_sensor = None
                    self.save()
            return None

        if len(werte) == 0:
            return None
        elif len(werte) == 1:
            wert = werte.values()[0]
        elif self.verknuepfung == '1':
            wert = min(werte.values())
        elif self.verknuepfung == '2':
            wert = max(werte.values())
        elif self.verknuepfung == '3':
            wert = sorted(werte.values())[len(werte)/2]
        elif self.verknuepfung == '4':
            wert = sum(werte.values()) / len(werte)
        elif self.verknuepfung == '5':
            wert = sum(sorted(werte.values())[1:]) / (len(werte)-1)
        elif self.verknuepfung == '6':
            wert = sum(sorted(werte.values())[:-1]) / (len(werte)-1)
        elif self.verknuepfung == '7':
            if len(werte) == 2:
                return None
            else:
                wert = sum(sorted(werte.values())[1:-1]) / (len(werte)-2)

        ch.set_new_temp(self.name, wert, self.haus_id)

        sig_new_temp_value.send(
            sender="vsensor", name=self.get_identifier(), value=float(wert),
            modules=self.haus.get_modules(), timestamp=datetime.now())

        return wert, now, self.haus_id

    def get_marker(self):
        marker = 'red'
        last = self.get_wert()
        if last:
            marker = 'green'
        return marker

    def get_verknuepfung(self):
        if self.verknuepfung == '1':
            return 'Minimalwert'
        elif self.verknuepfung == '2':
            return 'Maximalwert'
        elif self.verknuepfung == '3':
            return 'Medianwert'
        elif self.verknuepfung == '4':
            return 'Durchschnitt'
        elif self.verknuepfung == '5':
            return 'Durchschnitt ohne Minimum'
        elif self.verknuepfung == '6':
            return 'Durchschnitt ohne Maximum'
        elif self.verknuepfung == '7':
            return 'Durchschnitt ohne Minimum und Maximum'

    def get_backup_wert(self):
        try:
            return self.backup_sensor.get_wert()
        except (AttributeError, Sensor.DoesNotExist):
            return None

    def get_name_for_gw(self):
        if self.backup_sensor_id:
            return self.backup_sensor.name
        else:
            return None

    def get_link(self):
        return "<a style='color:#cb0963; white-space:normal;' target='_blank' href='/m_setup/" + str(self.haus_id) + \
               "/vsensoren/?vsedit=" + str(self.id) + "/'>%s</a>"

    @property
    def ctrl_device(self):
        return self.gateway

    class Meta:
        app_label = 'vsensoren'


@receiver(post_delete, sender=VSensor)
@receiver(post_save, sender=VSensor)
def _invalidate_sensor_cache(sender, instance, **kwargs):
    ch.delete(instance.get_identifier())


@receiver(post_delete, sender=Sensor)
def _sensor_delete(sender, instance, **kwargs):
    for vsensor in instance.haus.vsensor_related.all():
        if instance.get_identifier() in vsensor.input_sensoren:
            vsensor.input_sensoren = [s for s in vsensor.input_sensoren if instance.get_identifier() != s]
            vsensor.save()
        try:
            if instance == vsensor.backup_sensor:
                vsensor.backup_sensor = None
                vsensor.save()
        except Sensor.DoesNotExist:
            pass


@receiver(post_init, sender=Raum)
def _set_raum_vsensoren(sender, instance, **kwargs):
    vsensoren = VSensor.objects.filter(raum=instance)
    instance._sensoren += list(vsensoren)


@receiver(post_init, sender=Gateway)
def _set_gateway_vsensoren(sender, instance, **kwargs):
    sensoren = VSensor.objects.filter(gateway=instance)
    for s in sensoren:
        instance._sensoren.append(s)
