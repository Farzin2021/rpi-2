from django.apps import AppConfig


class WIFIConfig(AppConfig):
    name = 'wifi'
    verbose_name = 'Wi-Fi'

    def ready(self):
        pass
