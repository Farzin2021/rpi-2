from django.apps import AppConfig


class FunksollregelungConfig(AppConfig):
    name = 'funksollregelung'
    verbose_name = 'Funksollregelung'

    def ready(self):
        from . import signals
