# -*- coding: utf-8 -*-
from heizmanager.models import Haus, Raum, Regelung, Gateway, Sensor, Luftfeuchtigkeitssensor
from heizmanager.render import render_response, render_redirect
from rf.models import RFSensor, RFAktor, RFDevice
from knx.models import KNXSensor
from heizmanager.render import render_response
from fabric.api import local
from django.http import HttpResponse
from django.http import JsonResponse
import json
from itertools import chain
import urllib


def get_name():
    return "Logmonitor"


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/logmonitor/'>Logmonitor</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Nachvollziehbarkeit und Rückverfolgung, was das System, wann gemacht hat. "
    desc_link = "http://support.controme.com/logmonitor/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = params.get('logmonitor_right_menu', True)
        return render_response(request, "m_settings_logmonitor.html", context)

    if request.method == "POST":
        if 'right_menu_ajax' in request.POST:
            params = haus.get_module_parameters()
            params['logmonitor_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        return render_redirect(request, '/m_setup/%s/logmonitor/' % haus.id)


def get_logs(user, haus, mod, obj, **kwargs):
    search_q = kwargs.get('search_q')
    ret = []
    # celery / cron haben vollst pfad, uwsgi faengt in ~/rpi an
    hprg = "./temperaturszenen/views.py.get_local_settings_page\|"\
           "^/home/pi/rpi/heizprogramm/tasks.py.set_mode_for_raum\|"\
           "^./heizprogramm/views.py.get_local_settings_page_haus\|"\
           "/home/pi/rpi/temperaturszenen/views.py.set_mode_raum"
    modtrans = {
        'h': '^/home/pi/rpi/',
        'hfo': '\(^/home/pi/rpi/heizflaechenoptimierung/tasks.py\|^./heizflaechenoptimierung/tasks.py\).*\(|Status\||Modus\)',
        'doa': '^./\(ruecklaufregelung\|zweipunktregelung\)/views.py\|Solltemperatur\|Ventilstellung\|Zieltemperatur\|heizflaechenoptimierung/tasks.py.*|Ausgang \|./heizmanager/getandset.py.calc_opening.*|Ausgang ',
        'pl': '^./pumpenlogik/views.py',
        'mr': '^./vorlauftemperaturregelung/views.py',
        'dr': '^./differenzregelung/views.py',
        'fps': '^./fps/views.py',
        'hprg': hprg,
        'sys': '^/home/pi/rpi/dataslave/tasks.py.send_setup\|^./gatewaymonitor/views.py.do_ausgang',
        'gws': '^./heizmanager/getandset.py.tset_all',
        'sens': '^./heizmanager/getandset.py.tset_all\|^./rf/views.py.set_enocean\|^./rf/models.py.update_device\|^./knx/views.py.set_knx'
    }

    if mod in ['sens_servm', 'gws_servm']:
        return get_service_monitor_logs(user, haus, mod, obj, **kwargs)

    if obj == "h":
        raumdict = {"0": ""}
        if mod == "pl":
            for reg in Regelung.objects.filter(regelung="pumpenlogik"):
                raumdict[str(reg.id)] = reg.get_parameters().get('name') or "RA %s" % reg.id
        elif mod == "mr":
            for mid, params in haus.get_module_parameters().get('vorlauftemperaturregelung', dict()).items():
                raumdict[str(mid)] = params.get('name') or "VTR %s" % mid
        elif mod == "dr":
            for dregid, params in haus.get_module_parameters().get('differenzregelung', dict()).items():
                raumdict[str(dregid)] = params.get('name') or "DR %s" % dregid
        elif mod == "fps":
            for reg in Regelung.objects.filter(regelung="fps"):
                raumdict[str(reg.id)] = reg.get_parameters().get('name') or "FPS %s" % reg.id
        elif mod == "sys":
            pass
        elif mod == "gws":
            pass
        elif mod == 'sens':
            pass
        else:  # hfo / doa / hprg
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(user, etage_id=etage.id).only("name"):
                    raumdict[str(raum.id)] = u"%s / %s" % (etage.name, raum.name)

    try:
        log = local("grep '%s' /var/log/uwsgi/uwsgi.log | sort -r -k2 -t '|'" % modtrans[mod], capture=True)
    except:
        log = ''
    try:
        log1 = local("grep '%s' /var/log/uwsgi/uwsgi.log.1 | sort -r -k2 -t '|'" % modtrans[mod], capture=True)
        log += '\n' + log1
    except:
        pass
    log = log.split('\n')[:10000]

    import re
    if search_q:
        tmp = []
        for l in log:
            if re.findall(search_q, l, re.IGNORECASE):
                tmp.append(l)
        log = tmp
    
    if mod == "gws":
        for line in log:
            if not line.startswith("./"):
                continue
            l = line.split('|')
            if obj == 'h':
                ret.append([l[1], l[3]])
            elif l[3] == obj.strip():
                ret.append([l[1], "%s Sensoren verbunden" % (l[4].count(';')+1)])

    elif mod == "sens":
        obj = urllib.unquote(obj)
        if "#" in obj:
            gwmac = obj.split('#')[0]
            if len(obj.split('#')[1]) == 4:
                obj = obj[-4:-2] + '_' + obj[-2:]
            else:
                obj = obj[-6:-4] + '_' + obj[-4:-2] + '_' + obj[-2:]
        else:
            gwmac = None
        for line in log:
            if not line.startswith("./") or 'Solltemperatur' in line or 'Zieltemperatur' in line:
                continue
            l = line.decode('utf-8').split("|")
            if gwmac and l[3] != gwmac:
                continue
            if obj == "h":
                for s in l[4].split(';'):
                    ret.append([s.split(',')[0], l[1], l[3]])
            else:
                try:
                    _s = l[4].split(';')
                except IndexError:
                    _obj = obj.replace('\\', '')
                    if len(l) == 4 and l[3].startswith(_obj):
                        ret.append([l[1], l[3].split(' ', 1)[1]])
                    else:
                        continue
                else:  # u.a. 1wire
                    for s in _s:
                        sp = s.lower().split(',')
                        if sp[0] == obj.strip().replace('\\', ''):
                            try:
                                val = "%.2f" % float(sp[1])
                            except:
                                val = '-'
                            ret.append([l[1], val])

    else:
        for line in log:
            if not line.startswith("./") and not line.startswith("/home/pi/rpi/"):
                continue
            l = line.split('|')
            if obj == 'h':
                try:
                    ret.append([raumdict[l[2]]] + l[1::2])
                except:
                    # wenn ein Raum wieder geloescht wurde
                    pass
            elif l[2] == obj:
                ret.append(l[1::2])

    return ret


def get_service_monitor_logs(user, haus, mod, obj, **kwargs):
    search_q = kwargs.get('search_q')
    ret = []
    modtrans = {
        'gws_servm': '^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor|.*|Gateway|',
        'sens_servm': '^/home/pi/rpi/logmonitor/tasks.py.update_service_monitor|.*|Sensor|',
    }

    # re arrange to show and sort by month first in column 2
    re_arrange = "awk -F'|' 'OFS = \"|\" { split($2, a, \".\") && $2=a[2] \".\" a[1] \".\"  a[3]; print }'"

    grepfilter = ''
    if search_q:
        for s in search_q.split(','):
            grepfilter += " | grep %s" % s
    try:
        log = local("grep -a '%s' /var/log/uwsgi/service_monitor.log1 %s | %s | sort -r -k2 -t '|'"
                    % (modtrans[mod], grepfilter, re_arrange), capture=True)
    except:
        log = ''
    try:
        log1 = local("grep -a '%s' /var/log/uwsgi/service_monitor.log1.1 %s | %s | sort -r -k2 -t '|'" %
                     (modtrans[mod], grepfilter, re_arrange), capture=True)
        log += '\n' + log1
    except:
        pass
    log = log.split('\n')[:10000]

    for line in log:
        if not line.startswith("/home/pi/rpi/"):
            continue
        l = line.split('|')
        if len(l) > 3:
            if obj == 'h':
                ret.append([l[1], l[2], l[3], l[4]])
            elif l[3] == obj.strip():
                ret.append([l[1], l[2], l[3], l[4]])
    return ret


def get_local_settings_page_haus(request, haus):
    if request.method == "GET":
        if 'mod' in request.GET and 'obj' in request.GET:

            ret = get_logs(request.user, haus, request.GET['mod'], request.GET['obj'], search_q=request.GET.get('filter', ''))

            return HttpResponse(json.dumps(ret))

        else:
            room_ids = []
            eundr = []
            for etage in haus.etagen.all():
                e = [etage.name, []]
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id).only("name"):
                    e[1].append((raum.id, raum.name))
                    room_ids.append(raum.id)
                eundr.append(e)
            module = haus.get_modules()
            modtrans = {
                'heizflaechenoptimierung': ('hfo', 'Heizflächenoptimierung'),
                'pumpenlogik': ('pl', 'Raumanforderung'),
                'differenzregelung': ('dr', 'Differenzregelung'),
                'vorlauftemperaturregelung': ('mr', 'Vorlauftemperaturregelung'),
                'fps': ('fps', 'FPS'),
                'heizprogramm': ('hprg', 'Heizprogramm'),
            }  # supported modules
            modtrans_alwayson = [
                ('doa', 'ERR Ausgänge'),
                ('sys', 'Systemstatus'),
                ('sens', 'Sensoren'),
                ('sens_servm', 'Ausfallanzeige Sensoren'),
            ]
            gws = Gateway.objects.all()
            if len(gws):
                modtrans_alwayson.append(('gws', 'Gateways'))
                modtrans_alwayson.append(('gws_servm', 'Ausfallanzeige Gateways'))
            lmodules = [modtrans[m] for m in module if m in modtrans]
            lmodules += modtrans_alwayson

            abbrs = {'differenzregelung': 'dr', 'fps': 'fps', 'vorlauftemperaturregelung': 'mr', 'pumpenlogik': 'pl'}
            modoptions = dict((k, []) for k in abbrs.values())
            hparams = haus.get_module_parameters()
            for reg in Regelung.objects.filter(regelung__in=abbrs.keys()):
                if reg.regelung in {'fps', 'pumpenlogik'}:
                    t = (reg.get_parameters().get('name'), reg.id)
                else:
                    t = (hparams[reg.regelung][reg.get_parameters().values()[0]]['name'], reg.get_parameters().values()[0])
                if t not in modoptions[abbrs[reg.regelung]]:
                    modoptions[abbrs[reg.regelung]].append(t)
            
            for gw in gws:
                modoptions.setdefault('gws', list())
                modoptions['gws'].append(("%s%s" % (gw.name, (" %s" % gw.description if len(gw.description) else "")), gw.name))

            modoptions['gws_servm'] = modoptions.get('gws')
            modoptions.setdefault('sens', list())
            for sensor in sorted(list(chain(Sensor.objects.all(), Luftfeuchtigkeitssensor.objects.all(), RFSensor.objects.all(), RFAktor.objects.filter(type='wt'), RFAktor.objects.filter(type__startswith="hkt"), RFAktor.objects.filter(type__startswith="relais", protocol="enocean"), KNXSensor.objects.all())), key=lambda x: (room_ids.index(x.raum.id) if x.raum else None, x.name)):
                modoptions['sens'].append( (u"%s%s%s" % ("%s - %s" % (sensor.controller.name, sensor.name) if isinstance(sensor, RFDevice) and sensor.protocol == "zwave" else sensor.name, u" %s" % sensor.description if len(sensor.description) else "", u" - %s" % sensor.raum if sensor.raum else ""), "%s_%s" % (sensor.controller.name, sensor.name) if isinstance(sensor, RFDevice) and sensor.protocol == "zwave" else sensor.name) )

            modoptions['sens_servm'] = modoptions.get('sens')
            return render_response(request, "m_logmonitor.html", {'haus': haus, 'eundr': json.dumps(eundr), 'modules': sorted(lmodules, key=lambda x: x[1]), 'modoptions': json.dumps(modoptions)})
