from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from caching.base import CachingManager, CachingMixin

try:
    from django.contrib.contenttypes.fields import GenericForeignKey
except ImportError:
    from django.contrib.contenttypes.generic import GenericForeignKey


class ObjectPermissionManager(CachingManager):
    # aufpassen, dass die cache invalidation hier immer funktioniert

    def assign_perm(self, perm, user, obj):
        pass

    def remove_perm(self, perm, user, obj):
        pass

    def has_perm(self, perm, user, obj):
        ct = ContentType.objects.get_for_model(type(obj))
        ret = self.filter(content_type=ct, object_id=obj.pk, permission__gte=perm, user=user)
        if len(ret):
            return True
        else:
            return False


class ObjectPermission(CachingMixin, models.Model):
    # permissions in ascending order of extent
    PERMISSIONS = (
        ('0', 'read'),
        ('1', 'write'),
        ('2', 'delete'),
    )
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')  # ?

    permission = models.CharField(max_length=1, choices=PERMISSIONS)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    objects = ObjectPermissionManager()

    class Meta:
        app_label = 'benutzerverwaltung'
        unique_together = ['user', 'permission', 'object_id', 'content_type']  # wieso object_id und nicht content_object?

