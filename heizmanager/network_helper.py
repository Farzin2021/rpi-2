import socket


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def get_mac():
    with open('/sys/class/net/eth0/address') as f:
        mac = f.read()
        return mac.strip().replace(':', '-').lower()
