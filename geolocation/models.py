from django.db import models
from heizmanager.models import Haus, Raum
from raumgruppen.models import Raumgruppe
from django.contrib.contenttypes.models import ContentType
import logging
from caching.base import CachingManager, CachingMixin
import operator
from django.contrib.contenttypes.models import ContentType

try:
    from django.contrib.contenttypes.fields import GenericForeignKey
except ImportError:
    from django.contrib.contenttypes.generic import GenericForeignKey


class GeoDeviceManager(CachingManager):

    def get_all_offsets(self, haus, target_object):
        if target_object is None:
            ct = None
            objid = 0
        else:
            ct = ContentType.objects.get_for_model(type(target_object))
            objid = target_object.id
        devices = GeoDevice.objects.filter(haus=haus, content_type=ct, object_id=objid)
        ret = {}

        for device in devices:
            offsets = GeoOffset.objects.filter(haus=haus, device=device).order_by("-created")
            if len(offsets):
                ret[device] = offsets[0].offset

        return ret

    def get_active_offset(self, haus, target_object):

        ret = self.get_all_offsets(haus, target_object)

        if not len(ret):
            return None, None

        maxdev = max(ret.iteritems(), key=operator.itemgetter(1))[0]
        return maxdev, ret[maxdev]


class GeoDevice(CachingMixin, models.Model):
    uuid = models.CharField(max_length=36)
    name = (models.CharField(max_length=50))
    haus = models.ForeignKey(Haus)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True)  # fuer Raum/Raumgruppe
    object_id = models.PositiveIntegerField(default=0)
    content_object = GenericForeignKey('content_type', 'object_id')  # ?

    objects = GeoDeviceManager()

    def save(self, *args, **kwargs):
        if not self.uuid:
            import uuid
            self.uuid = uuid.uuid4()
        super(GeoDevice, self).save(*args, **kwargs)

    class Meta:
        app_label = 'geolocation'


class GeoOffset(CachingMixin, models.Model):
    haus = models.ForeignKey(Haus)
    device = models.ForeignKey(GeoDevice)
    offset = models.FloatField(default=None)
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        relevant_offsets = GeoOffset.objects.filter(device=self.device).order_by("-created").values_list("id")[:2]
        GeoOffset.objects.filter(device=self.device).exclude(pk__in=[r[0] for r in relevant_offsets]).delete()
        try:
            super(GeoOffset, self).save(*args, **kwargs)
        except:
            logging.exception("error saving GeoOffset")

    class Meta:
        app_label = 'geolocation'
