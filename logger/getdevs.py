from fabric.api import local
import time
import os

try:
    import pyudev
except ImportError:
    pass


context = pyudev.Context()

while True:

    if os.uname()[1] == 'contromeproserver':
        time.sleep(10)
        continue
    
    try:
        result = local("mount | grep usbmount", capture=True)
        for row in result.split('\n'):
            device = row.split(' ')[0]
            try:
                local("sudo fdisk -l | grep %s" % device, capture=True)
            except:
                try:
                    local("sudo umount %s" % device, capture=True)
                except:
                    # mist, mehrere devices auf demselben mountpoint
                    # neustarten ist vielleicht keine gute idee, wenn das einen anderen grund hat...
                    pass

        time.sleep(30) 

    except:

        for device in context.list_devices(subsystem='block', DEVTYPE='partition'):
            
            if 'mmcblk0p' not in device.device_node:

                try:
                    local("mkdir /home/pi/rpi/usbmount/")
                except:
                    pass

                try:
                    ret = local("mount | grep %s" % device.device_node, capture=True)
                except:
                    ret = ''

                if not len(ret):
                    local("sudo mount -t vfat -ouser,umask=0000 %s /home/pi/rpi/usbmount/" % device.device_node)
                elif len(ret) and '/home/pi/rpi/usbmount' not in ret:
                    local("sudo umount %s" % device.device_node)
                    local("sudo mount -t vfat %s /home/pi/rpi/usbmount/" % device.device_node)

        time.sleep(60)
