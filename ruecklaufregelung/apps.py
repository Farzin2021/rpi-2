# -*- coding: utf-8 -*-

from django.apps import AppConfig


class RuecklaufregelungConfig(AppConfig):
    name = 'ruecklaufregelung'
    verbose_name = u'Rücklaufregelung'

    def ready(self):
        from . import signals
