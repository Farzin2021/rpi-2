# -*- coding: utf-8 -*-

from heizmanager.models import Haus, Sensor, Gateway, GatewayAusgang, AbstractSensor, Regelung, Luftfeuchtigkeitssensor
from heizmanager.render import render_redirect, render_response
from django.http import HttpResponse
import heizmanager.cache_helper as ch
import json
import logging
from django.views.decorators.csrf import csrf_exempt
from heizmanager.models import sig_new_output_value, sig_new_temp_value, Raum
from heizmanager.mobile.m_temp import get_module_line_list_regelung, get_module_offsets_regelung
from benutzerverwaltung.decorators import has_regelung_access
from datetime import datetime, timedelta
import pytz
import operator
import math
import time
try:
    import wetter.views as wetter
except ImportError:
    wetter = None


logger = logging.getLogger('logmonitor')


def get_name():
    return u"Vorlauftemperaturregelung"


def managesoutputs():
    return False


def do_ausgang(regelung, ausgangno):
    haus = Haus.objects.all()[0]
    rparams = regelung.get_parameters()
    params = haus.get_module_parameters().get('vorlauftemperaturregelung', dict()).get(rparams['mid'])

    def get_last_value():
        last = ch.get("%s%sstatus" % (type(regelung).__name__, regelung.pk))
        return last['position'] if last is not None else None

    def get_setpoint():
        offset = get_module_offsets_regelung(haus, rparams['mid'], "vorlauftemperaturregelung")

        if params.get('vtrkind', 'weather') == 'weather':
            _, t = _get_ats_value(haus, params, params['ats'])
            if t is None:
                return None

            if t > 20:
                t = 20
            if t < -20:
                t = -20
            t = math.fabs(t-20)  # verschiebung analog zum diagram

            return float(params['offset']) + offset + (6.0 - float(params['slope'])) * math.sqrt(t**float(params['bend']))

        elif params.get('vtrkind') == 'demand':
            rooms = params.get('selected_rooms', [])
            if not len(rooms):  # demand ohne raum geht nicht, dann alle
                rooms = [str(_id[0]) for _id in Raum.objects.all().values_list("id")]
            max_rlsoll = 0
            for roomid in rooms:
                rlsoll = ch.get("rlsoll_%s" % roomid)
                if rlsoll > max_rlsoll:
                    max_rlsoll = rlsoll

            return max_rlsoll + offset + max_rlsoll / 10.0

    def _simpleproportionalmc(params):

        ret = 0
        err = False
        status = {'control': params['name'], 'description': ''}

        try:
            last_value = get_last_value()
            if last_value is None:
                err = True
                status['msg'] = "No last value present, setting position to 50%"
                ret = 50

            else:

                setpoint = min(get_setpoint(), float(params['max_temp']))
                outflow = params['outs']
                try:
                    outflow = AbstractSensor.get_sensor(outflow).get_wert()[0]
                except:
                    outflow = None

                status['setpoint'] = setpoint
                status['outflow'] = outflow
                _, status['atsvalue'] = _get_ats_value(haus, params, params['ats'])

                if params.get('vtrtype', '3') == '3':
                    inflow_hot = params['inws']
                    try:
                        inflow_hot = AbstractSensor.get_sensor(inflow_hot).get_wert()[0]
                    except:
                        inflow_hot = None
                    inflow_cold = params['incs']
                    try:
                        inflow_cold = AbstractSensor.get_sensor(inflow_cold).get_wert()[0]
                    except:
                        inflow_cold = None
                    status['inflow_hot'] = inflow_hot
                    status['inflow_cold'] = inflow_cold

                if outflow > params['max_temp']:
                    ret = 0

                else:
                    gain = float(params.get('gain', 1.0))
                    drive_increment = float(params.get('drive_increment', 0.5))
                    max_increment = float(params.get('max_increment', 10))
                    hysteresis = float(params.get('hysteresis', 1.0))
                    last_status = ch.get('%s%sstatus' % (type(regelung).__name__, regelung.pk)) or {'outflow': outflow, 'setpoint': setpoint}

                    if outflow > (setpoint + hysteresis) or outflow < (setpoint - hysteresis):
                        error = setpoint - outflow  # negative when outflow > setpoint

                        d = error * gain
                        if math.fabs(d) > max_increment:
                            d = math.copysign(max_increment, d)  # absolute value of m_i with the sign of d
                        elif math.fabs(d) < drive_increment:
                            d = 0

                        delta_out = 0
                        if (last_status.get('setpoint', setpoint) - hysteresis) <= setpoint <= (last_status.get('setpoint', setpoint) + hysteresis):
                            delta_out = (outflow - last_status['outflow']) * gain

                        ret = last_value + d - delta_out

                    else:
                        ret = last_value

                    ret = max(0, min(100, ret))

        except Exception as e:
            logging.exception("mixercontrol._simpleproportionalmc")
            err = True
            ret = 0

        status['position'] = ret  # 0-100, not runtime-adjusted!
        status['error'] = err
        ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), status)
        return ret

    def _simpledirectmc(params):

        ret = 0
        err = False
        status = {'control': params['name'], 'description': ''}

        try:
            setpoint = min(get_setpoint(), float(params['max_temp']))
            # outflow = params['outflow']
            # outflow = Sensor.objects.get(name=outflow).get_value()
            inflow_hot = params['inws']
            try:
                inflow_hot = AbstractSensor.get_sensor(inflow_hot).get_wert()[0]
            except:
                inflow_hot = None
            inflow_cold = params['incs']
            try:
                inflow_cold = AbstractSensor.get_sensor(inflow_cold).get_wert()[0]
            except:
                inflow_hot = None

            status['setpoint'] = setpoint
            # status['outflow'] = outflow  # ist None in self.get_output_state()
            status['inflow_hot'] = inflow_hot
            status['inflow_cold'] = inflow_cold

            if inflow_hot < setpoint and inflow_cold < setpoint:
                if inflow_hot >= inflow_cold:
                    ret = 100
                else:
                    ret = 0

            elif inflow_hot > setpoint > inflow_cold:
                diff = inflow_hot - inflow_cold
                ret = 1-(inflow_hot-setpoint)/diff
                ret *= 100

            elif inflow_hot < setpoint < inflow_cold:
                diff = inflow_cold - inflow_hot
                ret = (inflow_cold-setpoint)/diff
                ret *= 100

            else:  # beide groesser
                if inflow_hot >= inflow_cold:
                    ret = 0
                else:
                    ret = 100

        except Exception as e:
            logging.exception("mixercontrol._simpledirectmc")
            err = True
            ret = 0

        status['position'] = ret
        status['error'] = err
        ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), status)
        return ret

    def _pid(params):

        ret = 0
        err = False
        status = {'control': params['name'], 'description': ''}

        try:
            last_value = get_last_value()
            if last_value is None:
                err = True
                status['msg'] = "No last value present, setting position to 50%"
                ret = 50

            else:
                setpoint = min(get_setpoint(), float(params['max_temp']))
                outflow = params['outs']
                try:
                    outflow = AbstractSensor.get_sensor(outflow).get_wert()[0]
                except:
                    outflow = None

                status['setpoint'] = setpoint
                status['outflow'] = outflow
                _, status['atsvalue'] = _get_ats_value(haus, params, params['ats'])

                if outflow > params['max_temp']:
                    ret = 0

                else:
                    controller = params['controller']
                    kp = float(params.get('kp, 1'))
                    ki = float(params.get('ki', 0))
                    kd = float(params.get('kd', 0))
                    try:
                        windupguard = 100 * 1/ki
                    except ZeroDivisionError:
                        windupguard = 0
                    error = setpoint - outflow
                    last_status = ch.get('%s%sstatus' % (type(regelung).__name__, regelung.pk)) or {'outflow': outflow, 'setpoint': setpoint, 'ts': time.time(), 'error': error, 'pi': 0}

                    if math.fabs(error) < params.get('hysteresis', 0) and last_status.get('position') is not None:
                        ret = last_status.get('position')
                        last_status.update(status)
                        ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), last_status)
                        return ret  # hier return, damit nicht unten %s%sstatus aktualisiert wird

                    else:
                        ct = time.time()
                        status['ts'] = ct
                        status['_error'] = error
                        dt = (ct - (last_status.get('ts') or time.time())) / params['scan_interval']

                        pi = last_status.get('pi') or 0
                        if dt >= 1:
                            dt = min(dt, max(3, float(params['runtime'])/float(params['scan_interval'])))  # rt/si maximal ~15
                            pp = kp * error

                            pi += error*dt
                            if pi < 0:
                                pi = 0
                            elif pi > windupguard:
                                pi = windupguard

                            pd = 0
                            if controller == 'pid':
                                error /= dt  # erst hier normalisieren wegen berechnung von pi
                                status['_error'] = error  # wert neu berechnet, also in status neu setzen
                                de = error - (last_status.get('_error') or 0)
                                pd = de  # /dt  # wird oben schon dividiert

                            ret = pp + ki*pi + kd*pd
                            ret = max(0, min(100, ret))

                        else:
                            ret = last_status.get('position')
                            if ret is None:
                                logging.warning("dt <= 1, position from ls is None, setting to 0")
                                ret = 0

                        status['pi'] = pi

        except Exception as e:
            logging.exception("mixercontrol._pid")
            err = True
            ret = 0

        status['position'] = ret  # 0-100, not runtime-adjusted!
        status['error'] = err
        ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), status)
        return ret

    def _010vmapping(params):
        ret = 0
        err = False
        status = {'control': params['name'], 'description': ''}

        try:
            last_value = get_last_value()
            if last_value is None:
                err = True
                status['msg'] = "No last value present, setting position to 50%"
                ret = 50

            else:
                setpoint = min(get_setpoint(), float(params['max_temp']))
                outflow = params['outs']
                try:
                    outflow = AbstractSensor.get_sensor(outflow).get_wert()[0]
                except:
                    outflow = None

                status['setpoint'] = setpoint
                status['outflow'] = outflow
                _, status['atsvalue'] = _get_ats_value(haus, params, params['ats'])
                
                if outflow and outflow > params['max_temp']:
                    ret = 0

                else:
                    min_voltage = params['min_voltage'] * 10.0
                    max_voltage = params['max_voltage'] * 10.0
                    min_voltage_temp = float(params['min_voltage_temp'])
                    max_voltage_temp = float(params['max_voltage_temp'])

                    if setpoint > max_voltage_temp:
                        ret = max_voltage
                    elif setpoint < min_voltage_temp:
                        ret = min_voltage
                    else:
                        ret = ((max_voltage_temp - setpoint) / (max_voltage_temp - min_voltage_temp)) 
                        ret = max_voltage - (max_voltage - min_voltage) * ret
        
        except Exception as e:
            logging.exception("mixercontrol._010v")
            err = True
            ret = 0

        status['position'] = ret  # 0-100, not runtime-adjusted!
        status['error'] = err
        ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), status)
        return ret

    outflow = params['outs']
    vtrtype = params.get('vtrtype', '3')
    min_voltage = params.get('min_voltage', 0) * 10.0
    max_voltage = params.get('max_voltage', 10) * 10.0
    release_condition = params.get('release_condition', '0')
    release_behavior = params.get('release_behavior', '0')
    controller = params.get('controller', 'normal')

    if release_condition != '0' and release_condition == 0:
        if release_behavior == '0' or release_behavior == 'stay':
            last_value = get_last_value()
            if last_value is None:
                value = 10
            else:
                value = last_value
        elif release_behavior == 'off':
            value = 0
        elif release_behavior == 'on':
            value = 100
        elif release_behavior == 'min_ana':
            value = int(min_voltage)
        elif release_behavior == 'max_ana':
            value = int(max_voltage)

        status = {'control': params['name'], 'description': ''}
        status['setpoint'] = min(get_setpoint(), float(params['max_temp']))
        status['position'] = value
        status['error'] = False
        # params = self.get_parameters()
        outflow = params['outflow']
        try:
            outflow = AbstractSensor.get_sensor(outflow).get_wert()[0]
        except:
            outflow = None
        status['outflow'] = outflow
        if params.get('vtrtype', '3') == '3':
            inflow_hot = params['inws']
            try:
                inflow_hot = AbstractSensor.get_sensor(inflow_hot).get_wert()[0]
            except:
                inflow_hot = None
            inflow_cold = params['incs']
            try:
                inflow_cold = AbstractSensor.get_sensor(inflow_cold).get_wert()[0]
            except:
                inflow_cold = None
            status['inflow_hot'] = inflow_hot
            status['inflow_cold'] = inflow_cold
        ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), status)

    else:

        if vtrtype == '4':
            value = _010vmapping(params)
        elif controller == 'normal':
            if vtrtype == '2' or (vtrtype == '3' and outflow is None):  # zweite bedingung ist legacy
                value = _simpledirectmc(params)
            elif vtrtype == '3':
                value = _simpleproportionalmc(params)
            elif vtrtype == '1':
                value = _simpleproportionalmc(params)
            else:
                value = 0
        elif controller == 'pi':
            value = _pid(params)
        elif controller == 'pid':
            value = _pid(params)
        else:
            value = 0

        if min_voltage > value >= 0:
            value = int(min_voltage)
            status = ch.get('%s%sstatus' % (type(regelung).__name__, regelung.pk))
            status['position'] = value
            ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), status)
        elif value > max_voltage:
            value = int(max_voltage)
            status = ch.get('%s%sstatus' % (type(regelung).__name__, regelung.pk))
            status['position'] = value
            ch.set('%s%sstatus' % (type(regelung).__name__, regelung.pk), status)
        else:
            pass

    status = ch.get('%s%sstatus'% (type(regelung).__name__, regelung.pk))
    ch.set("mischer__%s" % rparams['mid'], status['setpoint'])

    try:
        if status.get('setpoint') is not None:
            sig_new_output_value.send(sender="vorlauftemperaturregelung",
                                      name="mischerout_%s" % rparams['mid'],
                                      value=status['position'],
                                      ziel=status['setpoint'],
                                      parameters=str({}),  # todo
                                      timestamp=datetime.utcnow(),
                                      modules=[])

        if status.get('inflow_cold') is not None:
            sig_new_temp_value.send(sender="vorlauftemperaturregelung",
                                    name="mischer_%s_inflow_cold" % rparams['mid'],
                                    value=status['inflow_cold'],
                                    modules=[],
                                    timestamp=datetime.utcnow())

        if status.get('inflow_hot') is not None:
            sig_new_temp_value.send(sender="vorlauftemperaturregelung",
                                    name="mischer_%s_inflow_hot" % rparams['mid'],
                                    value=status['inflow_hot'],
                                    modules=[],
                                    timestamp=datetime.utcnow())

        if status.get('outflow') is not None:
            sig_new_temp_value.send(sender="vorlauftemperaturregelung",
                                    name="mischer_%s_outflow" % rparams['mid'],
                                    value=status['outflow'],
                                    modules=[],
                                    timestamp=datetime.utcnow())
        logger.warning(
            "%s|%s" % (rparams['mid'],
                u"%s%s%s%s%s" %
                (
                   (u"Eingang Rücklauf %.2f°. " % status.get('inflow_cold')) if status.get('inflow_cold') else "",
                   (u"Eingang Warm %.2f°. " % status.get('inflow_hot')) if status.get('inflow_hot') else "",
                   (u"Vorlauf %.2f°. " % status.get('outflow')) if status.get('outflow') else "",
                   (u"Ziel %.2f°. " % status.get('setpoint')) if status.get('setpoint') else "",
                   (u"Position %s%%." % int(status.get('position'))) if status.get('position') is not None else "")
)
        )
    except Exception:
        logging.exception("loggerexc in vtr.do_ausgang")

    return HttpResponse("<%s>" % value)


def get_outputs(haus, raum, regelung):

    belegte_ausgaenge = dict()
    ausgaenge = GatewayAusgang.objects.filter(regelung__regelung='vorlauftemperaturregelung').select_related('gateway')
    params = haus.get_module_parameters()

    for mid, values in params.get('vorlauftemperaturregelung', dict()).items():
        for ausgang in ausgaenge:
            if ausgang.gateway_id == values.get('hrgw'):
                outputs = values.get('outputs', dict())
                belegte_ausgaenge.setdefault(ausgang.gateway, list())
                for name, no in outputs.items():  # todo: nur don/doff bzw ana nehmen, abhaengig vom output_type!
                    for n in no:
                        belegte_ausgaenge[ausgang.gateway].append((int(n), ausgang, "mischer_%s_%s" % (mid, name), True))

    return belegte_ausgaenge


def get_possible_outputs(haus):

    ctrldevices = [gw for gw in Gateway.objects.filter(haus=haus) if gw.is_heizraumgw() or "6.00" > gw.version >= "5.00"]
    regler = dict((cd, list()) for cd in ctrldevices)

    params = haus.get_module_parameters()
    for mid, values in params.get('vorlauftemperaturregelung', dict()).items():
        outs = AbstractSensor.get_sensor(values.get('outs'))
        inws = AbstractSensor.get_sensor(values.get('inws'))
        incs = AbstractSensor.get_sensor(values.get('incs'))
        vtrtype = values.get('vtrtype', '3')

        if outs is None and inws is None and incs is None and vtrtype != '4':
            continue

        if vtrtype in {'1', '3'} and outs and outs.gateway not in regler:
            # das koennen wir machen, wenn wir sensoren zum hrgw hinuebertragen koennen
            # fuer den moment faengt das, dass mr/dr an normalem gw angezeigt wird
            continue
        elif vtrtype == '2' and (inws and incs) and (inws.gateway not in regler or incs.gateway not in regler):
            continue
        elif vtrtype == '4':
            try:
                gw = Gateway.objects.get(pk=int(values['hrgw']))
            except:
                continue
        else:
            gw = outs.gateway if outs and vtrtype in {'1', '3'} else incs.gateway if incs else inws.gateway if inws else None
            if gw is None:
                continue

        if values.get('output_type') == 'digital':
            regler[gw].append(["mischer_%s_don" % mid, u"Mischer %s: '%s': auf" % (mid, values['name'])])
            regler[gw].append(["mischer_%s_doff" % mid, u"Mischer %s: '%s': zu" % (mid, values['name'])])

        elif values.get('output_type') == 'analog':
            regler[gw].append(["mischer_%s_ana" % mid, u"Vorlauftemperaturregelung %s: '%s'" % (mid, values['name'])])

    return regler


def _get_ats_value(haus, hparams, ats):

    if ats == "online":
        _ats = None
        try:
            at = wetter.get_forecast(haus, 1)
        except AttributeError:
            at = None
        if at is not None:
            at = float(at[0][1])
        _atsvalue = at

    elif ats == "fbh":
        _ats = None
        try:
            s = AbstractSensor.get_sensor(hparams['ruecklaufregelung']['atsensor'])
            if s:
                last = s.get_wert()
            else:
                last = None

            berlin = pytz.timezone("Europe/Berlin")
            now = datetime.now(berlin)
            if last is not None and (now - last[1]).total_seconds() < 1800:
                at = last[0]
            else:
                at = None
        except AttributeError:
            at = None
        _atsvalue = at

    else:
        try:
            _ats = AbstractSensor.get_sensor(ats).name
        except:
            _ats = None
        _atsvalue = None

    return _ats, _atsvalue


def get_config(request, macaddr):

    # todo muss entweder gar nichts zurueckgeben (also einen fehler oder so, dass in jsonapi ein valueerror kommt) oder zumindest die mids
    # todo  dazu ggf freigabebedingungen und nicht-hrgw-lokale sensorenwerte

    if request.method == 'GET':
        ch.set_gw_ping(macaddr.lower())
        
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)
        ret = {}
        haus = gw.haus

        params = haus.get_module_parameters()

        for mid, values in params.get('vorlauftemperaturregelung', dict()).items():
            if values['hrgw'] == gw.id:
                try:
                    ret.setdefault(mid, dict())
                    ret[mid]['control'] = 'simpleproportionalmc'
                    ret[mid].update(values)
                    ret[mid]['hrgw'] = Gateway.objects.get(pk=values['hrgw']).name
                    try:
                        ret[mid]['incs'] = AbstractSensor.get_sensor(ret[mid]['incs']).name
                    except:
                        ret[mid]['incs'] = None
                    try:
                        ret[mid]['inws'] = AbstractSensor.get_sensor(ret[mid]['inws']).name
                    except:
                        ret[mid]['inws'] = None
                    try:
                        ret[mid]['outs'] = AbstractSensor.get_sensor(ret[mid]['outs']).name
                    except:
                        ret[mid]['outs'] = None

                    ret[mid]['ats'], ret[mid]['atsvalue'] = _get_ats_value(haus, params, ret[mid]['ats'])
                    if ret[mid].get('vtrkind', 'demand'):
                        rooms = ret[mid].get('selected_rooms', [])
                        if not len(rooms):  # demand ohne raum geht nicht, dann alle
                            rooms = [str(_id[0]) for _id in Raum.objects.all().values_list("id")]
                        max_rlsoll = 0
                        for roomid in rooms:
                            rlsoll = ch.get("rlsoll_%s" % roomid)
                            if rlsoll > max_rlsoll:
                                max_rlsoll = rlsoll
                        ret[mid]['max_rlsoll'] = max_rlsoll + max_rlsoll / 10.0
                    ret[mid]['module_offset'] = get_module_offsets_regelung(haus, mid, "vorlauftemperaturregelung")

                    release_condition = ret[mid].get('release_condition', '0')
                    if release_condition == "0":
                        pass
                    elif release_condition.startswith('mr_'):
                        hrgw = params.get('vorlauftemperaturregelung', dict()).get(release_condition[3:], dict()).get('hrgw')
                        if hrgw is None:
                            ret[mid]['release_condition'] = "0"
                            logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekanntes Heizraumgateway."))
                        else:
                            hrgw = Gateway.objects.get(pk=hrgw)
                            status = ch.get("mixer_%s" % hrgw.name)
                            if status:
                                ret[mid]['release_condition'] = status.get(release_condition.split('_')[1], dict()).get('position')
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung %s." % (u"erfüllt" if status['position'] > 0 else u"nicht erfüllt")))
                            else:
                                ret[mid]['release_condition'] = "0"
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekannter Zustand der Bedingung."))

                    elif release_condition.startswith('dr_'):
                        hrgw = params.get('differenzregelung', dict()).get(release_condition[3:], dict()).get('hrgw')
                        if hrgw is None:
                            ret[mid]['release_condition'] = "0"
                            logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekanntes Heizraumgateway."))
                        else:
                            hrgw = Gateway.objects.get(pk=hrgw)
                            status = ch.get("cc_%s" % hrgw.name)
                            if status:
                                ret[mid]['release_condition'] = status.get(release_condition.split('_')[1], dict()).get('position')
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung %s." % (u"erfüllt" if status['position'] > 0 else u"nicht erfüllt")))
                            else:
                                ret[mid]['release_condition'] = "0"
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekannter Zustand der Bedingung."))

                    elif release_condition.startswith('pl_'):
                        try:
                            reg = Regelung.objects.get(pk=long(release_condition.split('_')[1]))
                        except Regelung.DoesNotExist:
                            ret[mid]['release_condition'] = "0"
                            reg = None
                            logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekannte Raumanforderung."))
                        if reg is not None:
                            cachedout = None
                            try:
                                plgwa = GatewayAusgang.objects.get(regelung=reg)
                                for ausgang in plgwa.ausgang.split(', '):
                                    cachedout = ch.get("%s_%s" % (plgwa.gateway.name, int(ausgang.strip())))
                                    if cachedout is not None:
                                        break
                            except Exception:
                                logging.exception("exc getting pl out")

                            try:
                                if cachedout is not None:
                                    val = int(cachedout.translate(None, '<>!')) if isinstance(cachedout, str) and len(cachedout.translate(None, '<>!')) else cachedout
                                else:
                                    val = int(reg.do_ausgang(0).content.translate(None, '<>!'))
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung %s." % (u"erfüllt" if val > 0 else u"nicht erfüllt")))
                            except Exception:
                                logging.exception("error getting pl rc")
                                val = int(reg.get_parameters().get('default', 1))
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekannter Zustand der Raumanforderung. Verwende Fehlerwert der Raumanforderung: %s." % val))
                            ret[mid]['release_condition'] = val

                    elif release_condition.startswith('fps_'):
                        try:
                            reg = Regelung.objects.get(pk=long(release_condition.split('_')[1]))
                        except Regelung.DoesNotExist:
                            ret[mid]['release_condition'] = "0"
                            reg = None
                            logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekannte FPS."))
                        if reg is not None:
                            try:
                                val = int(reg.do_ausgang(0).content.translate(None, '<>!'))
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung %s." % (u"erfüllt" if val > 0 else u"nicht erfüllt")))
                            except:
                                logging.exception("error getting fps rc")
                                val = int(reg.get_parameters().get('error_value', 1))
                                logger.warning("%s|%s" % (mid, u"Freigabebedingung fehlerhaft, unbekannter Zustand der FPS. Verwende Fehlerwert der FPS: %s." % val))
                            ret[mid]['release_condition'] = val

                    else:
                        pass

                except Exception as e:
                    logging.exception("error in get_config")
                    if mid in ret:
                        del ret[mid]

        ret['sensors'] = [s[0] for s in list(Sensor.objects.filter(gateway=gw).values_list('name')) + list(Luftfeuchtigkeitssensor.objects.filter(gateway=gw).values_list('name')) if not s[0].endswith("pt1000")]
        ret['sensor_offsets'] = [(s[0], s[1]) for s in Sensor.objects.filter(gateway=gw).values_list('name', 'offset')]

        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(405)


@csrf_exempt
def set_status(request, macaddr):

    if request.method == "POST":
        ch.set_gw_ping(macaddr.lower())

        data = json.loads(request.body)
        data['gateway_ip'] = request.META['REMOTE_ADDR']

        _data = ch.get("mischer_%s" % macaddr)
        if _data is None or not isinstance(_data, dict):
            _data = data
        else:
            try:
                _data.update(data)
            except AttributeError:
                _data = data
        ch.set("mischer_%s" % macaddr, _data)

        logging.warning(data)
        for mid, vals in data.items():
            if type(vals) != dict:
                continue
            elif mid == "error":
                try:
                    gw = Gateway.objects.get(name=macaddr.lower())
                except Gateway.DoesNotExist:
                    pass
                else:
                    ausgaenge = GatewayAusgang.objects.filter(gateway=gw)
                    for ausgang in ausgaenge:
                        if ausgang.regelung and ausgang.regelung.regelung == "differenzregelung":
                            logger.warning("%s|%s" % (ausgang.regelung.id, data['error']))
                continue

            if 'setpoint' in vals and 'last_drive' in vals:
                sig_new_output_value.send(sender="vorlauftemperaturregelung",
                                          name="mischerout_%s" % mid,
                                          value=vals['position'],
                                          ziel=vals['setpoint'],
                                          parameters=str({}),  # todo
                                          timestamp=datetime.strptime(vals['last_drive'], "%Y-%m-%d %H:%M"),
                                          modules=[])

            if vals.get('inflow_cold') is None:
                try:
                    haus = Haus.objects.all()[0]
                    params = haus.get_module_parameters()
                    sid = params['vorlauftemperaturregelung'][mid]['incs']
                    data[mid]['inflow_cold'] = AbstractSensor.get_sensor(sid).get_wert()[0]
                    _data.update(data)
                    ch.set("mischer_%s" % macaddr, _data)
                except Exception as e:
                    logging.exception("exc getting sensor")
            if vals.get('inflow_cold') is not None:
                sig_new_temp_value.send(sender="vorlauftemperaturregelung",
                                        name="mischer_%s_inflow_cold" % mid,
                                        value=vals['inflow_cold'],
                                        modules=[],
                                        timestamp=datetime.strptime(vals['last_drive'], "%Y-%m-%d %H:%M"))  # last_drive, weil da der wert massgeblich war

            if vals.get('inflow_hot') is None:
                try:
                    haus = Haus.objects.all()[0]
                    params = haus.get_module_parameters()
                    sid = params['vorlauftemperaturregelung'][mid]['inws']
                    data[mid]['inflow_hot'] = AbstractSensor.get_sensor(sid).get_wert()[0]
                    _data.update(data)
                    ch.set("mischer_%s" % macaddr, _data)
                except Exception as e:
                    logging.exception("exc getting sensor")
            if vals.get('inflow_hot') is not None:
                sig_new_temp_value.send(sender="vorlauftemperaturregelung",
                                        name="mischer_%s_inflow_hot" % mid,
                                        value=vals['inflow_hot'],
                                        modules=[],
                                        timestamp=datetime.strptime(vals['last_drive'], "%Y-%m-%d %H:%M"))  # last_drive, weil da der wert massgeblich war

            if vals.get('outflow') is not None:
                sig_new_temp_value.send(sender="vorlauftemperaturregelung",
                                        name="mischer_%s_outflow" % mid,
                                        value=vals['outflow'],
                                        modules=[],
                                        timestamp=datetime.strptime(vals['last_drive'], "%Y-%m-%d %H:%M"))  # last_drive, weil da der wert massgeblich war

            logger.warning(
                "%s|%s" % (mid,
                           u"%s%s%s%s%s" %
                           (
                               (u"Eingang Rücklauf %.2f°. " % vals.get('inflow_cold')) if vals.get('inflow_cold') else "",
                               (u"Eingang Warm %.2f°. " % vals.get('inflow_hot')) if vals.get('inflow_hot') else "",
                               (u"Vorlauf %.2f°. " % vals.get('outflow')) if vals.get('outflow') else "",
                               (u"Ziel %.2f°. " % vals.get('setpoint')) if vals.get('setpoint') else "",
                               (u"Position %s%%." % int(vals.get('position'))) if vals.get('position') is not None else "")
                           )
            )

            if vals.get('error', False) and len(vals.get('msg', '')):
                logger.warning(
                    "%s|%s" % (mid,
                               u"Fehler: %s" % vals.get('msg'))
                )

        return HttpResponse()

    else:
        return HttpResponse(status=405)


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/vorlauftemperaturregelung/'>Vorlauftemperaturregelung</a>" % haus.id


def get_global_description_link():
    desc = u"Steuerung der Vorlauftemperatur."
    desc_link = "http://support.controme.com/vorlauftemperatur-mischer/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    if request.method == "GET":

        if action == "edit":

            ret = dict(haus=haus)

            hparams = haus.get_module_parameters()
            drs = hparams.get('differenzregelung', dict())
            mrs = hparams.get('vorlauftemperaturregelung', dict())
            pls = Regelung.objects.filter(regelung="pumpenlogik")
            fps = Regelung.objects.filter(regelung="fps")
            regs = []
            for regid, params in drs.items():
                regs.append({
                    "name": params['name'],
                    "regid": "dr_" + regid,
                    "regtype": 'Differenzregelung'
                })
            for regid, params in mrs.items():
                if str(regid) == entityid:
                    continue
                regs.append({
                    "name": params['name'],
                    "regid": "mr_" + regid,
                    "regtype": 'Vorlauftemperaturregelung'
                })
            for pl in pls:
                if pl.get_ausgang():
                    regs.append({
                        "name": pl.get_parameters()['name'],
                        "regid": "pl_%s" % pl.pk,
                        "regtype": "Raumanforderung"
                    })
            for f in fps:
                params = f.get_parameters()
                regs.append({
                    "name": params['name'],
                    "regid": "fps_%s" % f.pk,
                    "regtype": "FPS"
                })
            ret['regs'] = sorted(regs, key=lambda x: "%s %s" % (x['regtype'], x['name']))

            ret['hrgws'] = [gw for gw in Gateway.objects.filter(haus=haus) if gw.is_heizraumgw() or "6.00" > gw.version >= "5.00"]
            sensors = Sensor.objects.filter(haus=haus).order_by("gateway_id", "name")  # todo: nur sensoren am jeweiligen hrgw zulassen?
            ret['sensoren'] = sensors
            ret['sensors_info'] = AbstractSensor.get_all_sensors_info(sensors=sensors)

            ret['mid'] = entityid
            ret.update({
                "name": "",
                "offset": 30.0,
                "slope": 5.5,
                "bend": 2.0,
                "runtime": 120,
                "scan_interval": 30,
                "hysteresis": 1.0,
                "gain": 1.0,
                'max_increment': 10,
                'drive_increment': 0.5,
                'max_temp': 50,
                'hrgw': 0,
                'min_voltage': 0,
                'max_voltage': 10,
                'calibration_interval': 1,
                'controller': 'normal',
                'kp': 1,
                'ki': 0,
                'kd': 0,
                'min_voltage_temp': 0,
                'max_voltage_temp': 0,
                'select_all_rooms': True,
                'vtrkind': 'weather'
            })

            if entityid:
                params = haus.get_module_parameters()
                ret.update(params.get('vorlauftemperaturregelung', dict()).get(entityid, dict()))
            else:
                ret['modules_list'] = ['vorlauftemperaturkorrektur', 'jahreskalender', 'timer']

            ret['icons'] = [
                '3-sensors_blue', 'buffer_yellow', 'floor-valve_blue', 'heater_blue', 'heating-valve_yellow',
                'pumpe_pink', 'thermometer_blue', '3-sensors_green', 'cloud', 'floor-valve_green', 'heater_green',
                'mischer_blue', 'pumpe_yellow', 'thermometer_green', '3-sensors_pink', 'floor-heating_blue',
                'floor-valve_pink', 'heater_pink', 'mischer_green', 'solar_blue', 'thermometer_pink',
                '3-sensors_yellow', 'floor-heating_green', 'heat-generator_blue', 'heater_yellow', 'mischer_pink',
                'solar_green', 'thermometer_yellow', 'buffer_blue', 'floor-heating_pink', 'heat-generator_green',
                'heating-valve_blue', 'mischer_yellow', 'solar_pink', 'buffer_green', 'floor-heating_yellow',
                'heat-generator_pink', 'heating-valve_green', 'pumpe_blue', 'solar_yellow', 'buffer_pink',
                'floor-valce_yellow', 'heat-generator_yellow', 'heating-valve_pink', 'pumpe_green', 'sun'
            ]

            eundr = []
            all_rooms = []
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(_raum)
                    all_rooms.append(str(_raum.id))
                eundr.append(e)
            ret['eundr'] = eundr
            if 'selected_rooms' not in ret:
                ret['selected_rooms'] = all_rooms
            elif len(ret['selected_rooms']) < len(all_rooms):
                ret['select_all_rooms'] = False

            return render_response(request, "m_settings_vorlauftemperaturregelung_edit.html", ret)

        elif action == "delete":
            regs = Regelung.objects.filter(regelung='vorlauftemperaturregelung')
            for reg in regs:
                regparams = reg.get_parameters()
                if regparams.get('mid', '0') == entityid:
                    try:
                        reg.gatewayausgang.delete()
                    except:
                        pass
                    reg.delete()

            params = haus.get_module_parameters()
            try:
                del params['vorlauftemperaturregelung'][entityid]
                haus.set_module_parameters(params)
            except:
                pass

            # clearing cache for upcoming offset
            from heizmanager.modules.module import planning_modules
            for planned_mod in planning_modules:
                ch.delete_cache_upcoming_events(haus.id, planned_mod, entityid, 'vorlauftemperaturregelung')
            ###
            return render_redirect(request, "/m_setup/%s/vorlauftemperaturregelung/" % haus.id)

        else:
            # das wird eine neue seite mit liste
            params = haus.get_module_parameters()
            return render_response(request, "m_settings_vorlauftemperaturregelung.html",
                                   {"haus": haus,
                                    "mischer": sorted(params.get('vorlauftemperaturregelung', dict()).iteritems())})

    elif request.method == "POST":

        logging.warning(request.POST)

        mid = request.POST.get('mischer', '0')
        params = haus.get_module_parameters()
        mischer = params.get('vorlauftemperaturregelung', dict())
        err = ''
        if mid == '0':
            ids = [0] + [int(k) for k in mischer.keys()]
            mid = str(max(ids) + 1)
        else:
            if mid in params['vorlauftemperaturregelung']:
                outs = params['vorlauftemperaturregelung'][mid].get('outputs', dict()).get('ana', list()) + \
                       params['vorlauftemperaturregelung'][mid].get('outputs', dict()).get('don', list()) + \
                       params['vorlauftemperaturregelung'][mid].get('outputs', dict()).get('doff', list())
                if params['vorlauftemperaturregelung'][mid]['output_type'] != request.POST.get('output_type') and len(outs):
                    err += u'Vor Änderung der Ausgangsart bitte bereits zugeordnete Ausgänge entfernen.\n'

        vtrtype = request.POST.get('vtrtype')
        vtrkind = request.POST.get('vtrkind', 'weather')
        icon = request.POST.get('icon', "")
        lst = []
        for key in request.POST:
            if key.startswith('c_'):
                k = key.split('_')
                lst.append(k[1])

        try:
            if request.POST.get('output_type') == 'digital' and int(request.POST.get('scan_interval', 300)) < 10:
                err += u'Abtastzeit muss größer sein als 10s.\n<br/>'
        except:
            err += u'Bitte numerische Werte für Laufzeit und Abtastinterval angeben.\n<br/>'

        if (vtrtype == '3' and len({request.POST.get('in_warm'), request.POST.get('in_cold'), request.POST.get('outs')}) < 3) or \
                (vtrtype == '2' and len({request.POST.get('in_warm'), request.POST.get('in_cold')}) < 2):
            err += u'Sensoren am Mischer müssen unterschiedlich sein.\n<br/>'

        if request.POST.get('output_type') == 'analog':
            try:
                min_voltage = float(request.POST.get('min_voltage', 0))
            except (TypeError, ValueError):
                min_voltage = 0.0
            try:
                max_voltage = float(request.POST.get('max_voltage', 10))
            except (TypeError, ValueError):
                max_voltage = 10.0
            if min_voltage < 0 or max_voltage > 10:
                err += u"Minimaler Analogwert muss mindestens 0, maximaler Analogwert darf höchstens 10 sein.\n<br/>"
            if min_voltage > max_voltage:
                err += u"Minimaler Analogwert muss kleiner sein als der maximale Analogwert.\n<br/>"
        else:
            min_voltage = 0
            max_voltage = 10
        
        name = request.POST['name']
        if not len(name):
            err += 'bitte Namen eingeben.\n<br/>'

        if vtrtype == '4':
            try:
                hrgw = int(request.POST.get('hrgw'))
            except (ValueError, TypeError):
                hrgw = None
            
            if hrgw is None:
                err += u'bitte HRGW auswählen.\n<br/>'

            try:
                min_voltage_temp = float(request.POST.get('min_voltage_temp'))
                max_voltage_temp = float(request.POST.get('max_voltage_temp'))
            except (TypeError, ValueError):
                err += u'bitte (ganze) Zahlen für Temperaturen bei minimalem/maximalem Analogwert angeben.\n<br/>'
            else:
                if min_voltage_temp >= max_voltage_temp:
                    err += u'Temperatur bei minimalem Analogwert muss größer als Temperatur bei maximalem Analogwert sein.\n<br/>'

        if len(err):
            ret = dict(haus=haus, error=err)

            ret['hrgws'] = [gw for gw in Gateway.objects.filter(haus=haus) if gw.is_heizraumgw() or "6.00" > gw.version >= "5.00"]
            ret['sensoren'] = Sensor.objects.filter(haus=haus).order_by("gateway_id", "name")  # todo: nur sensoren am jeweiligen hrgw zulassen?
            ret['vtrtype'] = vtrtype

            ret['mid'] = None
            if mid != '0':
                ret['mid'] = mid
                params = haus.get_module_parameters()
                ret.update(params.get('vorlauftemperaturregelung', dict()).get(mid, dict()))
            else:
                ret.update({
                    "name": "",
                    "offset": 30.0,
                    "slope": 5.5,
                    "bend": 2.0,
                    "runtime": 120,
                    "scan_interval": 300,
                    "hysteresis": 1.0,
                    "gain": 1.0,
                    'max_increment': 10,
                    'drive_increment': 0.5,
                    'max_temp': 50,
                    'hrgw': 0,
                    'min_voltage': 0,
                    'max_voltage': 10,
                    'calibration_interval': 7,
                    'controller': 'normal',
                    'kp': 1,
                    'ki': 0,
                    'kd': 0,
                    'min_voltage_temp': 0,
                    'max_voltage_temp': 0,
                    'show_in_menu': True
                })
            return render_response(request, "m_settings_vorlauftemperaturregelung_edit.html", ret)

        params.setdefault('vorlauftemperaturregelung', dict())
        params['vorlauftemperaturregelung'].setdefault(mid, dict())
        params['vorlauftemperaturregelung'][mid]['offset'] = request.POST.get('offset', 30.0)
        params['vorlauftemperaturregelung'][mid]['slope'] = request.POST.get('slope', 5.5)
        params['vorlauftemperaturregelung'][mid]['bend'] = request.POST.get('bend', 2.0)
        params['vorlauftemperaturregelung'][mid]['inws'] = request.POST.get('in_warm')
        params['vorlauftemperaturregelung'][mid]['incs'] = request.POST.get('in_cold')
        params['vorlauftemperaturregelung'][mid]['outs'] = request.POST.get('outs')
        params['vorlauftemperaturregelung'][mid]['ats'] = request.POST.get('ats')
        params['vorlauftemperaturregelung'][mid]['output_type'] = request.POST.get('output_type')
        params['vorlauftemperaturregelung'][mid]['runtime'] = request.POST.get('runtime', 120)
        params['vorlauftemperaturregelung'][mid]['scan_interval'] = request.POST.get('scan_interval', 300)
        params['vorlauftemperaturregelung'][mid]['hysteresis'] = request.POST.get('hysteresis', 1.0)
        params['vorlauftemperaturregelung'][mid]['gain'] = request.POST.get('gain', 1.0)
        params['vorlauftemperaturregelung'][mid]['max_increment'] = request.POST.get('max_increment', 10)
        params['vorlauftemperaturregelung'][mid]['drive_increment'] = request.POST.get('drive_increment', 0.5)
        params['vorlauftemperaturregelung'][mid]['max_temp'] = request.POST.get('max_temp', 50)
        params['vorlauftemperaturregelung'][mid]['hrgw'] = int(request.POST.get('hrgw', 0))
        params['vorlauftemperaturregelung'][mid]['name'] = request.POST.get('name', mid)
        params['vorlauftemperaturregelung'][mid]['vtrtype'] = vtrtype
        params['vorlauftemperaturregelung'][mid]['min_voltage'] = min_voltage
        params['vorlauftemperaturregelung'][mid]['max_voltage'] = max_voltage
        params['vorlauftemperaturregelung'][mid]['release_condition'] = request.POST.get('release_condition', "0")
        params['vorlauftemperaturregelung'][mid]['release_behavior'] = request.POST.get('release_behavior', "0")
        params['vorlauftemperaturregelung'][mid]['calibration_interval'] = request.POST.get('calibration_interval', 7)
        params['vorlauftemperaturregelung'][mid]['icon'] = icon
        params['vorlauftemperaturregelung'][mid]['modules_list'] = lst
        params['vorlauftemperaturregelung'][mid]['controller'] = request.POST.get('controller', 'normal')
        params['vorlauftemperaturregelung'][mid]['kp'] = request.POST.get('kp', 1) if len(request.POST.get('kp')) else 1
        params['vorlauftemperaturregelung'][mid]['ki'] = request.POST.get('ki', 0) if len(request.POST.get('ki')) else 0
        params['vorlauftemperaturregelung'][mid]['kd'] = request.POST.get('kd', 0) if len(request.POST.get('kd')) else 0
        params['vorlauftemperaturregelung'][mid]['min_voltage_temp'] = request.POST.get('min_voltage_temp', 0)
        params['vorlauftemperaturregelung'][mid]['max_voltage_temp'] = request.POST.get('max_voltage_temp', 0)
        params['vorlauftemperaturregelung'][mid]['show_in_menu'] = bool(request.POST.get('show_in_menu', 0))
        params['vorlauftemperaturregelung'][mid]['selected_rooms'] = request.POST.getlist('rooms')
        params['vorlauftemperaturregelung'][mid]['vtrkind'] = vtrkind
        haus.set_module_parameters(params)

        for reg in Regelung.objects.filter(regelung='vorlauftemperaturregelung'):
            regparams = reg.get_parameters()
            if regparams['mid'] == mid:
                break
        else:
            reg = Regelung(regelung='vorlauftemperaturregelung', parameter="{'mid': '%s'}" % mid)
            reg.save()
        return render_redirect(request, "/m_setup/%s/vorlauftemperaturregelung/" % haus.id)


@has_regelung_access
def get_local_settings_page_haus(request, haus, action=None, objid=None):

    if request.method == "GET":

        if action == 'show':
            hparams = haus.get_module_parameters()
            config = hparams.get('vorlauftemperaturregelung', dict).get(objid)
            if config is None:
                err = u'Ungültige Vorlauftemperaturregelung.'
                return render_response(request, "m_vorlauftemperaturregelung_show.html", {'haus': haus, 'error': err, 'logging': 'logger' in haus.get_modules()})

            try:
                hrgw = Gateway.objects.get(pk=config['hrgw'])
            except Gateway.DoesNotExist:
                hrgw = None

            context = {}
            context['logging'] = 'logger' in haus.get_modules()
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            yd = (now - timedelta(days=1)).strftime("%Y-%m-%dT00:00")
            now = now.strftime("%Y-%m-%dT%H:%M")
            context['von'] = yd
            context['bis'] = now

            regelung_offsets = get_module_offsets_regelung(haus, objid, 'vorlauftemperaturregelung', as_dict=True)
            regelung_offset = sum(regelung_offsets.values())
            module_lines = get_module_line_list_regelung(haus, objid, 'vorlauftemperaturregelung')
            context['mods'] = module_lines
            context['haus'] = haus
            context['config'] = config
            context['objid'] = objid

            try:
                hysteresis = float(config.get('hysteresis', 1))
            except ValueError:
                hysteresis = 1

            context['ziel_center'] = hysteresis / 2

            config['min_slider_val'] = config.get('min_slider_val', 10)
            config['max_slider_val'] = config.get('max_slider_val', 60)

            if regelung_offsets:
                offsets_items = dict((mod, abs(offset)) for mod, offset in regelung_offsets.items())
                max_module = max(offsets_items.iteritems(), key=operator.itemgetter(1))
            else:
                max_module = 0
            context['max_icon'] = max_module
            context['offset'] = sum(regelung_offsets.values()) or 0.0
            context['offset_converted'] = (abs(float(context['offset'])) * 100) / 100
            config['hysteresis'] = float(config['hysteresis'])

            if hrgw is not None and hrgw.is_heizraumgw():
                states = ch.get("mischer_%s" % hrgw.name if hrgw else '_%s' % objid)
            else:
                reg = Regelung.objects.get(parameter="{'mid': '%s'}" % objid, regelung="vorlauftemperaturregelung")
                status = ch.get("%s%sstatus" % (type(reg).__name__, reg.pk))
                states = {objid: status} if status is not None else None
            if states is not None:
                status = states.get(objid, {})
                if 'atsvalue' in status:
                    try:
                        atsvalue = float(status.get('atsvalue'))
                        atsvalue = min(atsvalue, 100)
                        if atsvalue < 0:
                            atsvalue = 0
                    except TypeError:
                        atsvalue = None
                    context['atsvalue_converted'] = atsvalue
                if 'inflow_hot' in status:
                    try:
                        inflow_hot = float(status.get('inflow_hot'))
                        inflow_hot = min(inflow_hot, 100)
                        if inflow_hot < 0:
                            inflow_hot = 0
                    except TypeError:
                        inflow_hot = None
                    context['inflow_hot_converted'] = inflow_hot
                if 'position' in status:
                    try:
                        position = float(status.get('position'))
                        position = min(position, 100)
                        if position < 0:
                            position = 0
                    except TypeError:
                        position = None
                    context['position_converted'] = position
                if 'inflow_cold' in status:
                    try:
                        inflow_cold = float(status.get('inflow_cold'))
                        inflow_cold = min(inflow_cold, 100)
                        if inflow_cold < 0:
                            inflow_cold = 0
                    except TypeError:
                        inflow_cold = None
                    context['inflow_cold_converted'] = inflow_cold
                if status and 'atsvalue' not in status:  # freigabebedingung nicht erfuellt
                    _, status['atsvalue'] = _get_ats_value(haus, hparams, config['ats'])
            else:
                status = {}
                context['atsvalue_converted'] = 0
                context['position_converted'] = 0
                context['inflow_cold_converted'] = 0
                context['inflow_hot_converted'] = 0
            temp = "%.2f" % status['outflow'] if status and status.get('outflow') is not None else "-"
            visual_temp = 0
            try:
                visual_temp = float(temp)
                visual_temp = min(visual_temp, 100)
                if visual_temp < 0:
                    visual_temp = 0
            except:
                pass
            context['visual_temp'] = visual_temp
            context['state'] = status
            context['soll'] = ("%.2f" % (float(status.get('setpoint')) - float(context['offset']))) if status and status.get('setpoint') else "-"

            context['ziel'] = "%.2f" % status['setpoint'] if status and status.get('setpoint') else "-"

            overflow = True
            hidden_soll = False
            hidden_ziel = False

            context['soll'] = float(context['soll']) if context['soll'] != '-' else context['soll']
            if context['soll'] <= 0:
                context['soll_converted'] = 0
            elif context['soll'] == '-':
                hidden_soll = True
                context['soll_converted'] = 0
            elif context['soll'] >= 100:
                context['soll_converted'] = 100
            else:
                context['soll_converted'] = (float(context['soll']) * 100) / 100
                overflow = False

            context['ziel'] = float(context['ziel']) if context['soll'] != '-' else context['soll']
            if context['ziel'] <= 0:
                context['ziel_converted'] = 0
            elif context['ziel'] == '-':
                hidden_ziel = True
                context['ziel_converted'] = 0
            elif context['ziel'] >= 100:
                context['ziel_converted'] = 100
            else:
                context['ziel_converted'] = (float(context['ziel']) * 100) / 100
                overflow = False
            context['delta'] = 100 - context['soll_converted']
            context['overflow'] = overflow
            context['hidden_soll'] = hidden_soll
            context['hidden_ziel'] = hidden_ziel

            return render_response(request, "m_vorlauftemperaturregelung_show.html", context)
