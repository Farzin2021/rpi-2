from heizmanager.tests import BaseModeTest
from heizmanager.models import Haus, Raum
from .views import HeizprogrammTask


class HeizprogrammTest(BaseModeTest):

    def setUp(self):
        super(HeizprogrammTest, self).setUp('heizprogramm/fixtures/fixture.json')

    def test_HeizprogrammCorrectlySwitchesBetweenTwoTemperaturszenen(self):
        # mode 1(Nacht) is set as default in fixture
        self.assert_temperatureszene_is_set_in_db(3, 1)
        self.set_heizprogramm_entry_ui(2, self.berlin_now)
        day_of_week = self.berlin_now.weekday()
        str_now_time = self.berlin_now.strftime("%H:%M")
        self.assert_heizprogramm_is_set_db(2, str_now_time, day_of_week)
        self.assert_temperatureszene_is_activated_in_room(3, 1)
        self.run_auto_switch()
        self.assert_temperatureszene_is_set_in_db(3, 2)
        self.assert_temperatureszene_is_activated_in_room(3, 2)

    def test_HeizprogrammDoesNotSwitchIfItIsDeactivated(self):
        self.active_heizprogramm_ui(1)
        self.assert_temperatureszene_is_set_in_db(3, 1)
        self.set_heizprogramm_entry_ui(2, self.berlin_now)
        day_of_week = self.berlin_now.weekday()
        str_now_time = self.berlin_now.strftime("%H:%M")
        self.assert_heizprogramm_is_set_db(2, str_now_time, day_of_week)
        self.assert_temperatureszene_is_activated_in_room(3, 1)
        self.run_auto_switch()
        self.assert_temperatureszene_is_set_in_db(3, 1)
        self.assert_temperatureszene_is_activated_in_room(3, 1)

    def test_HeizprogrammSwitchDoesNotOverrideAnActiveHandMode(self):
        self.set_hand_mode_single_room_ui(room_id=3, slidernumber=20.99, duration=180)
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.set_heizprogramm_entry_ui(2, self.berlin_now)
        day_of_week = self.berlin_now.weekday()
        str_now_time = self.berlin_now.strftime("%H:%M")
        self.assert_heizprogramm_is_set_db(2, str_now_time, day_of_week)
        self.run_auto_switch()
        # temperatureszene should be set in db correctly to be activated after hand mode expiration
        self.assert_temperatureszene_is_set_in_db(3, 2)
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.assert_raum_solltemp_equal(3, 20.99)

    def test_HeizprogrammDoesNotSwitchToInactiveTemperaturszene(self):
        raum = Raum.objects.get(id=3)
        r_params = raum.get_module_parameters()
        r_params['temperaturmodi'][2]['enabled'] = False
        raum.set_module_parameters(r_params)
        # mode 1(Nacht) is set as default in fixture
        self.assert_temperatureszene_is_set_in_db(3, 1)
        self.set_heizprogramm_entry_ui(2, self.berlin_now)
        day_of_week = self.berlin_now.weekday()
        str_now_time = self.berlin_now.strftime("%H:%M")
        self.assert_heizprogramm_is_set_db(2, str_now_time, day_of_week)
        self.assert_temperatureszene_is_activated_in_room(3, 1)
        self.run_auto_switch()
        # activated should be still 1 and not 2
        self.assert_temperatureszene_is_set_in_db(3, 1)
        self.assert_temperatureszene_is_activated_in_room(3, 1)

    def test_GetModuleTasksFunctionality(self):
        haus = self.get_haus_by_id(1)
        self.set_heizprogramm_entry_ui(2)
        module_tasks = HeizprogrammTask.get_module_tasks(haus)
        self.assertEqual(len(module_tasks), 1)
        self.run_modules_timer()
        self.assert_temperatureszene_is_activated_in_room(3, 2)


