from django.apps import AppConfig


class TelegramBotConfig(AppConfig):
    name = 'telegrambot'
    verbose_name = 'controme heizmanager telegrambot'

    def ready(self):
        from views import connect_signals
        connect_signals(None)
