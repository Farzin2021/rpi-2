from django.conf.urls import url
from . import views
from .tasks import periodic_evaluation_of_kilog


urlpatterns = [ 
    #url(r'^m_setup/(?P<haus>\d+)/ki/(?P<action>[a-z_]+)/$', views.get_global_settings_page),
    #url(r'^m_setup/(?P<haus>\d+)/ki/(?P<action>[a-z_]+)/(?P<entityid>\d+)/$', views.get_global_settings_page),
    url(r'^ki/calc-rated/$', periodic_evaluation_of_kilog),
    ]
