from django.apps import AppConfig


class NullRegelungConfig(AppConfig):
    name = 'nullregelung'
    verbose_name = 'Nullregelung'

    def ready(self):
        from . import signals
