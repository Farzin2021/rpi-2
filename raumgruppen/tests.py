from django.test import TestCase, RequestFactory, Client
from heizmanager.models import Haus, HausProfil, Etage, Raum, Regelung
from models import Raumgruppe
from django.contrib.auth.models import User
from django.core.cache import cache as memcache
import views as rgviews
import logging


class RGTest(TestCase):

    def setUp(self):
        self.usr = User.objects.create_user(username='test@test.de', email='test@test.de', password='test')
        self.usr.save()
        self.haus = Haus(name="Teststr. 1, 12345 Testhausen", eigentuemer=self.usr)
        self.haus.save()
        hausprofil = HausProfil(haus=self.haus, modules='raumgruppen', module_parameters=str({}))
        hausprofil.save()
        etage = Etage(name='testetage', haus=self.haus, position=0)
        etage.save()
        self.raum1 = Raum(name='testraum1', etage=etage, position=0, solltemp=21.0, module_parameters='{}')
        self.raum1.save()
        self.raum2 = Raum(name='testraum2', etage=etage, position=0, solltemp=21.0, module_parameters='{}')
        self.raum2.save()
        self.raum3 = Raum(name='testraum3', etage=etage, position=0, solltemp=21.0, module_parameters='{}')
        self.raum3.save()
        self.raum4 = Raum(name='testraum4', etage=etage, position=0, solltemp=21.0, module_parameters='{}')
        self.raum4.save()

        # params = {'active': True, 'rlsensorssn': {self.rlsensor1.get_identifier(): [1]}, 'neigung': 5.5, 'offset': 20, 'kruemmung': 2.0, 'schaerfe': 0.0, 'intervall': 600}
        self.reg = Regelung(regelung='zweipunktregelung', parameter=str({'delta': 0.0, 'active': True}))
        self.reg.save()
        self.zrreg = Regelung(regelung='zweipunktregelung', parameter=str({'delta': 0.0, 'active': True}))
        self.zrreg.save()
        self.raum1.regelung = self.reg
        self.raum1.save()
        self.raum2.regelung = self.zrreg
        self.raum2.save()

        memcache._cache.flush_all()

        self.client = Client()
        self.client.login(username='test@test.de', password='test')

    def testCreateRG(self):
        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgcreate': 'true', 'rsel1': 'on', 'rsel2': 'on', 'name': 'testrg'})
        self.assertEqual(len(Raumgruppe.objects.all()), 1)

    def testEditRGAdd(self):
        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgcreate': 'true', 'rsel1': 'on', 'rsel2': 'on', 'name': 'testrg'})
        rg = Raumgruppe.objects.all()[0]

        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgedit': 'true', 'rgid': str(rg.pk), 'rgname': 'edittest'})
        rg = Raumgruppe.objects.all()
        self.assertEqual(len(rg), 1)
        rg = Raumgruppe.objects.filter(haus=self.haus)[0]
        self.assertEqual(rg.name, 'edittest')

        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgedit': 'true', 'rgid': str(rg.pk), 'rgname': 'edittest', 'rsel3': 'on'})
        rg = Raumgruppe.objects.filter(haus=self.haus)[0]
        self.assertEqual(len(rg.raeume), 3)

    def testEditRGDel(self):
        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgcreate': 'true', 'rsel1': 'on', 'rsel2': 'on', 'name': 'testrg'})
        rg = Raumgruppe.objects.all()[0]

        response = self.client.get('/m_setup/%s/raumgruppen/?rgid=%s&rid=%s' % (self.haus.pk, rg.pk, self.raum2.pk))
        rg = Raumgruppe.objects.all()[0]
        self.assertEqual(rg.raeume, ['1'])

        response = self.client.get('/m_setup/%s/raumgruppen/?rgid=%s&rid=%s' % (self.haus.pk, rg.pk, self.raum1.pk))
        rg = Raumgruppe.objects.all()[0]
        self.assertEqual(rg.raeume, [])

        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgedit': 'true', 'rgid': str(rg.pk), 'rgname': 'edittest', 'rsel3': 'on'})
        rg = Raumgruppe.objects.filter(haus=self.haus)[0]
        self.assertEqual(rg.raeume, ['3'])

    def testSetSoll(self):
        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgcreate': 'true', 'rsel1': 'on', 'rsel2': 'on', 'name': 'testrg'})
        rg = Raumgruppe.objects.all()[0]

        response = self.client.post('/m_raumgruppen/%s/' % rg.pk,
                                    {'temp': '1', 'soll': '22.5', 'sollwert': '22.5', 'soffsetval': '0.0'})
        r1 = Raum.objects.get(name='testraum1')
        self.assertEqual(r1.solltemp, 22.5)
        r2 = Raum.objects.get(name='testraum2')
        self.assertEqual(r2.solltemp, 22.5)

    def testSetSolloffset(self):
        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgcreate': 'true', 'rsel1': 'on', 'rsel2': 'on', 'name': 'testrg'})
        rg = Raumgruppe.objects.all()[0]

        response = self.client.post('/m_raumgruppen/%s/' % rg.pk,
                                    {'soll': '21.0', 'sollwert': '21.0', 'soffsetval': '-0.8', 'solloffset': '1'})
        r1 = Raum.objects.get(name='testraum1')
        self.assertEqual(r1.solltemp, 20.2)
        r2 = Raum.objects.get(name='testraum2')
        self.assertEqual(r2.solltemp, 20.2)

